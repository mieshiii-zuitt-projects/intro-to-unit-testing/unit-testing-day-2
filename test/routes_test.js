const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');

chai.use(http);

describe('API tests', () => {
    it('TEST API Get people is running', () => {
        chai.request('http://localhost:5000')
        .get('/people')
        .end((err, res) => {
            expect(res).to.not.equal(undefined)
        });
    });

    it('TEST API Get people returns 200', () => {
        chai.request('http://localhost:5000')
        .get('/people')
        .end((err, res) => {
            expect(res.status).to.equal(200)
        });
    });

    it('TEST API Post person returns 400 if no NAME property', (done) => {
        chai.request('http://localhost:5000')
        .post('/person')
        .type('json')
        .send({ 
            alias: "Mikasa",
            age: 27
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        });
    });
    //Activity Solutions
    //A
    it('TEST API Post person is running', () => {
        chai.request('http://localhost:5000')
        .get('/person')
        .end((err, res) => {
            expect(res).to.not.equal(null)
        });
    });
    //B
    it('TEST API Post person encounters an error if no ALIAS', (done) => {
        chai.request('http://localhost:5000')
        .post('/person')
        .type('json')
        .send({ 
            name: "Armin",
            age: 29
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        });
    });
    //C
    it('TEST API Post person returns 400 if no AGE property', (done) => {
        chai.request('http://localhost:5000')
        .post('/person')
        .type('json')
        .send({ 
            name: "Mikasa"
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        });
    });
});