const { factorial, div_check } = require('../src/util');
const { expect, assert } = require('chai');

describe('Test Factorials', () => {
    it('Test that 5! is 120', () => {
        const product = factorial(5);
        expect(product).to.equal(120);
    });

    it('Test that 1! is 1', () => {
        const product = factorial(1);
        assert.equal(product, 1);
    });

    //Solution 1
    it('Test that 0! is 1', () => {
        const product = factorial(1);
        assert.equal(product, 1);
    });

    it('Test that 4! is 24', () => {
        const product = factorial(4);
        assert.equal(product, 24);
    });

    it('Test that 10! is 3628800', () => {
        const product = factorial(10);
        assert.equal(product, 3628800);
    });

    //S04 activity
    //Test if input is non-numeric
    it('Test that input is non-numeric', () => {
        const input = factorial('a');
        assert.notEqual(typeof input, undefined);
    })
});

describe('Test div_check', () => {
    // Test 105 is Divisible by 5
    it('Test that 105 is divisible by 5', () => {
        const divisible = div_check(105);
        assert.equal(divisible, true);
    });
    // Test if 14 is Divisible by 7
    it('Test that 14 is divisible by 7', () => {
        const divisible = div_check(14);
        assert.equal(divisible, true);
    });
    // Test if 0 is Divisible by 5 or 7
    it('Test that 0 is divisible by 5 or 7', () => {
        const divisible = div_check(0);
        assert.equal(divisible, true);
    });
    // Test if 22 is not Divisible by 5 or 7
    it('Test that 22 is NOT divisible by 5 or 7', () => {
        const divisible = div_check(22);
        assert.notEqual(divisible, true);
    });
});