const { names } = require('../src/util');

module.exports = (app) => {
    app.get('/', (req, res) => {
        return res.send({data: {}});
    });

    app.get('/people', (req, res) => {
        return res.send({
            people: names
        });
    });

    app.post('/person', (req, res) => {
        if(!req.body.hasOwnProperty('name')){
            return res.status(400).send({
                'error': 'Bad request: missing required parameter NAME'
            });
        };
        if(typeof req.body.name !== 'string'){
            return res.status(400).send({
                'error': 'Bad request: NAME has to be string'
            });
        };
        if(!req.body.hasOwnProperty('age')){
            return res.status(400).send({
                'error': 'Bad request: missing required parameter AGE'
            });
        };
        if(typeof req.body.age !== 'number'){
            return res.status(400).send({
                'error': 'Bad request: AGE has to be number'
            });
        };
        if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error' : 'Bad Request: Alias is missing property'
			});
		};
    });
};